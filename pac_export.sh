#!/bin/bash

pacman -Q|cut -f 1 -d " " > $(date --iso-8601)$(echo _packages).txt
mv $(date --iso-8601)$(echo _packages).txt pkg_list/$(date --iso-8601)$(echo _packages).txt
